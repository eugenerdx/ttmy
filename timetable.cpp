#include "timetable.h"
#include "ui_timetable.h"
#include <stdio.h>
#include <QMessageBox>


TimeTable::TimeTable(QWidget *parent) :  QWidget(parent), ui(new Ui::TimeTable)
{
    ui->setupUi(this);
    tableView= ui->tableView;
    model = new QStandardItemModel(5, 4, tableView);
    QStringList headers;
    headers<<"Subject"<<"Room"<<"Clock"<<"Day";
    model->setHorizontalHeaderLabels(headers);
    tableView->setModel(model);
    //QString *array = new QString [1024];

    QString array[] = {
        "/Users/eugenerdx/Documents/Resources/Lists/ListOfSubjects.list",
        "/Users/eugenerdx/Documents/Resources/Lists/ListOfHours.list",
        "/Users/eugenerdx/Documents/Resources/Lists/ListOfAuditoriums.list"
        ,"/Users/eugenerdx/Documents/Resources/Lists/ListOfWeekdays.list"};
    for(int i=0; i < 4; i++)
    {
        loadData(array[i], i);
        //QMessageBox::Information(this, "", QString::number(i));
    }
 //   pbAdd = new QPushButton(tr(" Add"));
    //connect(pbAdd, SIGNAL(clicked()), this, SLOT(onClickButtonAdd()));
}

   void TimeTable::loadData(QString& adress, int i)
   {
       QStandardItemModel *modelItems  = new QStandardItemModel();
       
       QFile file(adress);

       if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
       {
           QMessageBox::information(this,"File","File not found");
       }
       QTextStream in(&file);
       QStandardItem * item;
       while(!in.atEnd())
       {
            QString line = in.readLine();
            item=new QStandardItem(line);
            modelItems->appendRow(item);
       }
       switch (i)
       {
       case 0:    ui->lvSubject->setModel(modelItems);
           break;
       case 1:    ui->lvClock->setModel(modelItems);
           break;
       case 2:    ui->lvRoom->setModel(modelItems);
           break;
       case 3:    ui->lvDays->setModel(modelItems);
           break;
       }
       file.close();
   }


   void TimeTable::onClickButtonAdd()
   {
     //  QListView List();
     //  QModelIndex index = List.modelItems()->index(0, 0);

       //=(ui->lvSubject->selectedIndexes().first()).toString
      // connect(pbAdd, SIGNAL(clicked)), this, SLOT(onClickButtonAdd());

   }


TimeTable::~TimeTable()
{
    delete ui;
}

void TimeTable::on_pbAdd_clicked()
{
    QModelIndexList list=ui->lvSubject->selectionModel()->selectedIndexes();
    foreach(const QModelIndex& index,list)
    {
         QString line = index.data(Qt::DisplayRole).toString();

          QMessageBox::information(this,"",line);
    }
}
