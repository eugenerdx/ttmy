#-------------------------------------------------
#
# Project created by QtCreator 2016-02-02T02:40:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TImeTable5
TEMPLATE = app


SOURCES += main.cpp\
        timetable.cpp

HEADERS  += timetable.h

FORMS    += timetable.ui
