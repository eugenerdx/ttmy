#include "timetable.h"
#include <QApplication>
#include <iostream>
#include <fstream>
#include <string>




void printListController(std::ifstream& fin, const std::string& fileName)
{
    fin.open(fileName.c_str());

    if (!fin.is_open())
    {
        std::cout << "Error open file: " << fileName << std::endl;
        return;
    }

    std::string temp;
    std::cout << fileName << ':' << std::endl;
    while (std::getline(fin, temp))		std::cout << temp << std::endl;
    std::cout << std::endl;
    fin.close();
}

int printList()
{
    std::ifstream fin;
    std::string filePath = "Resources/Lists/";

    std::string fileNameSubjects = "ListOfSubjects.list";
    std::string fileNameAuditoriums = "ListOfAuditoriums.list";
    std::string fileHours = "ListOfHours.list";
    std::string fileWeekdays = "ListOfWeekdays.list";

    printListController(fin, filePath + fileNameSubjects);
    printListController(fin, filePath + fileNameAuditoriums);
    printListController(fin, filePath + fileHours);
    printListController(fin, filePath + fileWeekdays);

    system("PAUSE");
    return 0;
}

/*
void TimeTable::loadData()
{

QString fileName = QFileDialog::getOpenFileName(this,
        "Open File", "Resources/Lists/", "LIST FILES (*.list)");
QFile inputFile(fileName);
if (inputFile.open(QIODevice::ReadOnly))
{
    QTextStream in(&inputFile);
    while(!in.atEnd())
    {
        QString line = in.readLine();
        Items = new QStandardItem(QString(line).arg(i));
        ListModel->appendRow(Items);
        ui->lvSubject->setModel(ListModel);
    }
    inputFile.close();
    }


QStandardItem * Items;
QStandardItemModel* ListModel = new QStandardItemModel();
for(int i=0 ; i <10; i++)
{
    Items = new QStandardItem(QString(line).arg(i));
    ListModel->appendRow(Items);
    ui->lvSubject->setModel(ListModel);
}


   }*/

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TimeTable w;
    w.show();

    return a.exec();
}


