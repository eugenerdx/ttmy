#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>
#include <QListView>
#include <QMainWindow>
#include <QApplication>
#include <QStandardItem>
#include <QGridLayout>
#include <QVector>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QPushButton>

namespace Ui {
class TimeTable;
}

class TimeTable : public QWidget
{
    Q_OBJECT

public:
    explicit TimeTable(QWidget *parent = 0);
    ~TimeTable();
   // void loadData();
    QStandardItemModel *model;
    QStandardItemModel *modelItems;
    void loadData(QString& adress, int i);
    void on_keywordsList_clicked(const QModelIndex &index);

private:
    Ui::TimeTable *ui;
    QTableView *tableView;
    QListView * listView;
    QPushButton * pbAdd;
private slots:
    void onClickButtonAdd();
    void on_pbAdd_clicked();
};

#endif // TIMETABLE_H
